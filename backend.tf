terraform {
  backend "gcs" {
    bucket      = "tfstate-mba-iesp"
    prefix      = "dev/state"
    credentials = "user.json"
  }
}